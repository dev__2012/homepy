import argparse
import pathlib

from app import settings
from src import cloud, management

parser = argparse.ArgumentParser(prog="main.py", description="Make backups easily.")

parser.add_argument("--make-backup", "-mb", help="create backup zip file", action=argparse.BooleanOptionalAction)
parser.add_argument("--send-to-cloud",
                    "-sc",
                    help="send backup zip file to gcloud",
                    action=argparse.BooleanOptionalAction)
parser.add_argument("--clean-local-backups",
                    "-clb",
                    help="delete all directories/files on backups path",
                    action=argparse.BooleanOptionalAction)
parser.add_argument("--clean-cloud-backups",
                    "-ccb",
                    help="delete all backups on gcloud",
                    action=argparse.BooleanOptionalAction)
parser.add_argument("-mbsc", help="create backup zip file and send to cloud", action=argparse.BooleanOptionalAction)

args = parser.parse_args()

if not args.mbsc:
    if args.make_backup:
        management.Manager().do_backup()

    if args.send_to_cloud:
        zip_file = settings.BASE_DIR.joinpath(pathlib.Path("backups")).joinpath(settings.DEST_FILE)

        cloud.Cloud().upload_file(pathlib.Path(str(zip_file.absolute()) + ".zip"))
else:
    management.Manager().do_backup()
    zip_file = settings.BASE_DIR.joinpath(pathlib.Path("backups")).joinpath(settings.DEST_FILE)
    cloud.Cloud().upload_file(pathlib.Path(str(zip_file.absolute()) + ".zip"))

if args.clean_local_backups:
    management.Manager().clean_backups_dir()

if args.clean_cloud_backups:
    cloud.Cloud().clean_backups()
