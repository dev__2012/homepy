import datetime
import pathlib

import decouple

BASE_DIR = pathlib.Path(__file__).resolve().parent.parent

GCLOUD_CREDENTIALS = {
    "client_id": decouple.config("CLIENT_ID"),
    "client_secret": decouple.config("CLIENT_SECRET"),
    "refresh_token": decouple.config("REFRESH_TOKEN")
}

HOME: pathlib.Path = pathlib.Path(decouple.config("USER_HOME"))

IMPORTANT_DIRECTORIES: list[pathlib.Path] = [
    pathlib.Path("Documentos"),
]

PATTERNS_TO_IGNORE: list[str] = [
    "desktop.ini", "node_modules", "Meus Vídeos", "Minhas Imagens", "Minhas Músicas", "backups", "backup"
]

DEST_FILE = pathlib.Path(f'backup_{datetime.datetime.now().strftime("%Y_%m_%d")}')
