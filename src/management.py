import pathlib
import shutil
import sys

from app import settings

shutil.copystat = lambda x, *_, **__: x


class Manager:

    def __init__(self) -> None:
        self.__temp_dir = settings.BASE_DIR.joinpath(pathlib.Path("backup"))
        self.__dest_backups = settings.BASE_DIR.joinpath(pathlib.Path("backups"))
        self.__dest_file = self.__dest_backups.joinpath(settings.DEST_FILE)

    def copy_home_tree(self) -> None:
        for directory in settings.IMPORTANT_DIRECTORIES:
            print(f"[❗] Copying {directory}..." + " "*100, end="\r")

            shutil.copytree(src=settings.HOME.joinpath(directory),
                            dst=self.__temp_dir.joinpath(directory),
                            ignore=shutil.ignore_patterns(*settings.PATTERNS_TO_IGNORE))

            print(f"[👌] Copying {directory} OK!" + " "*100, end="\r")

    def clean_temp_backup_dir(self) -> None:
        try:
            shutil.rmtree(self.__temp_dir)
        except FileNotFoundError:
            ...

        print("[👌] Clean Temporary Backup Directory OK!" + " "*100)

    def clean_backups_dir(self) -> None:
        try:
            for dir in self.__dest_backups.iterdir():
                print(f"[❗] Removing {dir.name}..." + " "*100, end="\r")
                if dir.is_file():
                    dir.unlink()
                elif dir.is_dir():
                    shutil.rmtree(dir)
        except FileNotFoundError:
            ...

        print("[👌] Clean Backups Directory OK!" + " "*100)

    def make_backup_zip_file(self) -> None:
        print("[❗] Making zip file..." + " "*100, end="\r")

        shutil.make_archive(self.__dest_file, "zip", self.__temp_dir)

        print("[👌] Make Zip File OK!" + " "*100)

    def do_backup(self) -> None:
        try:
            self.copy_home_tree()

            self.make_backup_zip_file()

            print("[❗] Cleaning temporary directories..." + " "*100, end="\r")

            shutil.rmtree(self.__temp_dir)
        except KeyboardInterrupt:
            print(f"[🛑] Process Stopped by User!" + " "*100)
            self.clean_temp_backup_dir()
            self.clean_backups_dir()
            sys.exit(0)

        print(f"[👌] Success!, output file: {self.__dest_file.name}" + " "*100)
