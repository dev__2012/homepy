import pathlib

from google.oauth2 import credentials as g_credentials
from googleapiclient import discovery as g_discovery
from googleapiclient import http as g_http

from app import settings


class Cloud:

    def __init__(self) -> None:
        self.__service = g_discovery.build("drive",
                                           "v3",
                                           credentials=g_credentials.Credentials.from_authorized_user_info(
                                               settings.GCLOUD_CREDENTIALS))

    def print_backups(self) -> None:
        for file in self.__service.files().list(q="name contains 'backup'").execute()["files"]:
            print(file)

    def delete_file(self, file_id: str) -> None:
        self.__service.files().delete(fileId=file_id).execute()

    def upload_file(self, file_path: pathlib.Path) -> None:
        print(f"[🚀] Sending {file_path.name} to cloud..." + " "*100, end="\r")
        self.__service.files().create(body={
            "name": file_path.name
        }, media_body=g_http.MediaFileUpload(file_path)).execute()

        print(f"[👌] Send {file_path.name} To Cloud OK!" + " "*100)

    def clean_backups(self) -> None:
        for file in self.__service.files().list(q="name contains 'backup'").execute()["files"]:
            print(f"[❗] Cleaning {file.get('name')}..." + " "*100, end="\r")
            self.delete_file(file.get('id'))

        print("[👌] Clean Cloud Backups OK!" + " "*100)
